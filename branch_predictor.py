import math
import numpy as np
import matplotlib.pyplot as plt
def branch_predictor(addr,W,table_size,h_len,SR,R,G,H,out):
    
    j=h_len
    i=addr%table_size
    
    pred=SR[0]+W[i,0]
    if(pred>=0):prediction=1
    else: prediction=-1
    for a in range(0,j-1):
        #Speculative partial sum
        if(prediction>=0):
            SR[a]=SR[a+1]+W[i,a+1]
        else:
            SR[a]=SR[a+1]-W[i,a+1]
        #non Speculative partial sum
        if(out>0):
            R[a]=R[a+1]+W[i,a+1]
        else:
            R[a]=R[a+1]-W[i,a+1]
    #After branch outcome is known and prediction was wrong
    if(prediction!=out):    
        SR=R    
    else:
        R=SR          
    G=G[1:h_len] 
    H=H[1:h_len]
    G.append(out)
    H.append(i)
    return pred,W,SR,R,G,H

def train(out ,addr,table_size,h_len,W,G,H):
    index=addr%table_size
    W[index,0]=W[index,0]+out
    x=len(H)-1
    for j in range(1,h_len):
        
        if(out!=G[x]):
            W[H[x],j]=W[H[x],j]-1
        else:
            W[H[x],j]=W[H[x],j]+1    
        x-=1    
    return W    


def simulate_predictor(table_size,h_len):
    
    #PC=[]
    W=np.zeros((table_size,h_len)) #weights
    theta=27.51
    SR=[0 for i in range(h_len)] #speculative partial sum
    R=[0 for i in range(h_len)]  #non-speculative partial sum
    G=[1 for i in range(h_len)] #branch predictor history
     
    H=[0 for i in range(h_len)]  #previous branch index history
    incorrect=0
    with open("branch-trace-gcc1.trace",'r') as file:
        print('--------------------------------------------------|',end="\r",flush=True)
        for i in range(16000000):
            ins=file.readline()
            PC=int(ins[0:10],base=10)
            if(ins[11]=='T'):
                outcome=1
            elif(ins[11]=='N'):
                outcome=-1
            
            if(i%320000==0):
                print("#", end="",flush=True)
              
            prediction,W,SR,R,G,H=branch_predictor(PC,W,table_size,h_len,SR,R,G,H,outcome)    
            
            if(np.sign(prediction)!=outcome):
                #print("incorrect")
                incorrect+=1
                W=train(outcome,PC,table_size,h_len,W,G,H)  #update the weights table if prediction was wrong
            elif(abs(prediction)<=theta):
                #print("correct1")
                W=train(outcome,PC,table_size,h_len,W,G,H)  #update the weights table if predication was right but less than threshold
            else:
                pass
                #print("correct")
    accuracy= (1-(incorrect/16000000))*100
    print("incorrect:{0}  accuracy:{1} ".format(incorrect,accuracy))
    return accuracy
    
acur_arr=[]
tab_arr=[]    
for x in range(1,6):
    accuracy=simulate_predictor(1024*(2**x),7)
    tab_arr.append(1024*(2**x))
    acur_arr.append(accuracy)
plt.plot(tab_arr,acur_arr)
plt.xlabel("table size")

plt.ylabel("prediction accuracy")
plt.show()
    



                
